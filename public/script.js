var PRICE = 9.99;
var LOAD_NUM = 10;

new Vue({
    el: '#app',
    data: {
        Total: 0,
        items: [],
        result: [],
        cart: [],
        search: 'insult',
        loading:false,
        price: PRICE
    },
    computed: {
        noMoreItem: function(){
            return this.items.length === this.result.length && this.result.length > 0
        },
        total: function(){
            this.Total += PRICE;
        }
    },
    methods:{
        appendItems: function(){
            console.log('Ending Reached')
            if (this.items.length < this.result.length) {
                var append = this.result.slice(this.items.length, this.items.length + LOAD_NUM)
                this.items = this.items.concat(append)
            }
        },
        onSubmit: function(){
            if(this.search.length){
                this.items = [];
                this.loading = true;
                this.$http
                .get('/search/'.concat(this.search))
                .then(function(response){
                    this.result = response.data;
                    this.items = response.data;
                    this.appendItems();
                    this.loading = false
                })
            } else {
                console.log("Empty Search query")
            }
        },
        addItem: function(index){
            var item = this.items[index];
            var found = false;
            for (let i = 0; i < this.cart.length; i++) {
                if (this.cart[i].id === item.id) {
                    found = true
                    this.cart[i].qty++;
                    break;
                }
            }
            if (!found) {
                this.cart.push({
                    id: item.id,
                    title: item.title,
                    qty: 1,
                    price: item.score
                })
            }
            this.Total += PRICE;
            console.log(cart.price);
            
        },
        inc: function(item){
            item.qty++;
            this.Total += PRICE;
        },
        dec: function(item){
            item.qty--;
            this.Total -= PRICE;
            if(item.qty <= 0){
                for (var i = 0; i < this.cart.length; i++) {
                    if (this.cart[i].id === item.id) {
                        this.cart.splice(i, 1);
                        break;
                    }
                }
            }
        }
    },
    filters: {
        currency: function(price){
            return '$'.concat(price.toFixed(2))
        }
    },
    mounted: function() {
        this.onSubmit();

        var vueInstance = this;
        var elem = document.getElementById('product-list-bottom')
        var watcher = scrollMonitor.create(elem)
        watcher.enterViewport(function(){
            vueInstance.appendItems();
        })
    }
});
